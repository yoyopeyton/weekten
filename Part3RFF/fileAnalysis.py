#Peyton Oakes-Bryden
#17 November, 2015
#Period II

def unique_file():
#FileOpenBot
    input_file1 = open('/Users/poakes2016/Documents/10-WeekTen/3-1Input.txt', 'r')
    file_content1 = input_file1.read()
    input_file1.close()
    
    input_file2 = open('/Users/poakes2016/Documents/10-WeekTen/3-2Input.txt', 'r')
    file_content2 = input_file2.read()
    input_file2.close()
#FileSplitBot
    word_list1 = file_content1.split()
    word_list2 = file_content2.split()
    word_list3 = file_content1.split() + file_content2.split()
#OutputFileOpenBot
    file1 = open('/Users/poakes2016/Documents/10-WeekTen/3-1Output.txt', 'w')
    file2 = open('/Users/poakes2016/Documents/10-WeekTen/3-2Output.txt', 'w')
    file3 = open('/Users/poakes2016/Documents/10-WeekTen/3-3Output.txt', 'w')
#FileWriteBot
    unique_word1 = set(word_list1)
    for word in unique_word1:
        file1.write(str(word) + "\n")
    file1.close()
    
    unique_word2 = set(word_list2)
    for word in unique_word2:
        file2.write(str(word) + "\n")
    file2.close()
    
    unique_word3 = set(word_list3)
    for word in unique_word3:
        file3.write(str(word) + "\n")
    file3.close()
#ProgramRunnerBot   
unique_file()